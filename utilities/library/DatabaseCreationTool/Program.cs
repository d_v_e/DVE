﻿using MainDataHandler;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanDatabaseTool
{
    class Program
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args">First argument is xml file path, second is table name</param>
        static void Main(string[] args)
        {
            string connectString = "Server=ip-address; Port=portnumber; Database=dve_dev ;user=dve_dev; Password=password;
            DbConnection dbConnection = new MySqlConnection(connectString);
            XmlConverter xmlConverter = new XmlConverter(args[0], dbConnection, args[1]);

            if(args[2].Equals("new"))
            {
                try
                {
                    dbConnection.Open();
                    DbCommand dbCommand = dbConnection.CreateCommand();
                    string dropTableStatement = $"drop table if exists {args[1]}";
                    dbCommand.CommandText = dropTableStatement;
                    dbCommand.ExecuteNonQuery();
                }
                finally
                {
                    dbConnection.Close();
                }
                

                string creationStatement = $"create TABLE {args[1]}"
                + "("
                + "    id                         int auto_increment primary key,"
                + "    Trip_ID                    varchar(50) not null,"
                + "      Taxi_ID                    varchar(130) not null,"
                + "      Trip_Start_Timestamp       datetime null,"
                + "      Trip_End_Timestamp         datetime null,"
                + "      Trip_Seconds               int null,"
                + "      Trip_Miles                 double null,"
                + "      Pickup_Census_Tract        bigint null,"
                + "      Dropoff_Census_Tract       bigint null,"
                + "      Pickup_Community_Area      int null,"
                + "      Dropoff_Community_Area     int null,"
                + "      Fare                       double null,"
                + "      Tips                       double null,"
                + "      Tolls                      double null,"
                + "      Extras                     double null,"
                + "      Trip_Total                 double null,"
                + "      Payment_Type               enum('Credit Card','Cash','Mobile','Prcard','Unknown','No Charge','Dispute','Prepaid') not null,"
                + "      Company varchar(255) null,"
                + "      Pickup_Centroid_Latitude double not null,"
                + "     Pickup_Centroid_Longitude  double not null,"
                + "    Pickup_Centroid_Location   varchar(50) not null,"
                + "      Dropoff_Centroid_Latitude double not null,"
                + "     Dropoff_Centroid_Longitude double not null,"
                + "     Dropoff_Centroid__Location varchar(50) not null"
                + ");";

                xmlConverter.CreateCleanDatabase(creationStatement);

                //DataHandler dataHandler = new DataHandler(dbConnection);
            }


            List<string> notNullList = new List<string>();
            notNullList.Add("Trip_ID");
            notNullList.Add("Taxi_ID");
            notNullList.Add("Payment_Type");
            notNullList.Add("Pickup_Centroid_Latitude");
            notNullList.Add("Pickup_Centroid_Longitude");
            notNullList.Add("Pickup_Centroid_Location");
            notNullList.Add("Dropoff_Centroid_Latitude");
            notNullList.Add("Dropoff_Centroid_Longitude");
            notNullList.Add("Dropoff_Centroid_Location");

            xmlConverter.WriteXMLToCleanDatabase(notNullList);
        }
    }
}
