using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;

namespace MainDataHandler.DBControllers
{
    /// <summary>
    /// Controller voor de data tabel
    /// </summary>
   internal class DynamicTableController
    {
        private readonly DbConnection _dbConnection;

        /// <summary>
        /// Constructor of the DynamicTableController
        /// </summary>
        public DynamicTableController(DbConnection dbConnection)
        {
            this._dbConnection = dbConnection;
        }

        internal void ExecuteStatement(string SqlStatement)
        {
            try
            {
                this._dbConnection.Open();

                DbCommand command = this._dbConnection.CreateCommand();
                command.CommandText = SqlStatement;
                command.ExecuteNonQuery();
            }

            finally
            {
                this._dbConnection.Close();
            }
        }

        /// <summary>
        /// Add columns from the xml to the specified table of the database file
        /// </summary>
        /// <param name="dbFile">The database file to modify</param>
        /// <param name="xmlValueNames">The xml value names to add as columns</param>
        /// <param name="tableName">Name of the table to add columns to</param>
        internal void AddColumns(string dbFile, List<string> xmlValueNames, string tableName)
        {
            try
            {
                // Make a connection to the database
                this._dbConnection.Open();

                // Create new DbCommand
                DbCommand command = this._dbConnection.CreateCommand();

#if DEBUG
                Console.WriteLine("DEBUG:Added parameters");
#endif

                // Loop through each parameter (thus value names), add string to SQLite command
                string commandText;
                if (this._dbConnection is MySql.Data.MySqlClient.MySqlConnection)
                {
                    commandText = $"CREATE TABLE {tableName}(RowID INTEGER PRIMARY KEY AUTO_INCREMENT,";
                }
                else
                {
                    commandText = $"CREATE TABLE {tableName}(RowID INTEGER PRIMARY KEY AUTOINCREMENT,";
                }

                for (int i = 1; i < xmlValueNames.Count; i++)
                {
                    if (i == (xmlValueNames.Count - 1))
                    {
                        // Add a column name to the command
                        commandText = $"{commandText} {xmlValueNames[i]} TEXT)";
                    }
                    else
                    {
                        // Add a column name to the command
                        commandText = $"{commandText} {xmlValueNames[i]} TEXT,";
                    }
                }

#if DEBUG
                Console.WriteLine("DEBUG:Combined strings to make command");
#endif

                // Add command string as a parameter to command
                command.CommandText = commandText;

#if DEBUG
                Console.WriteLine($"INFO: Create Columns command = {commandText}");
#endif

                // Create table and execute command
                command.ExecuteNonQuery();
            }
            catch (DbException ex)  // Catch any SQLiteExceptions
            {
                throw new ArgumentException("ERROR: SQLiteException caught", ex);
            }
            finally // If the table is all set, close the connection
            {
                // If everything went as planned, close the connection
                this._dbConnection.Close();
                Console.WriteLine("DEBUG: Closed connection to database");
            }
        }

        /// <summary>
        /// Commit rows to the database table
        /// </summary>
        /// <param name="queryBuilder">The queryBuilder with the querydata</param>
        internal void PutRows(StringBuilder queryBuilder)
        {
            // Try to make a connection to the database, and execute command to write to the database
            try
            {
                this._dbConnection.Open();
#if DEBUG
                Console.WriteLine("DEBUG:Opened connection to database");
#endif

                DbCommand command = this._dbConnection.CreateCommand();

#if DEBUG
                Console.WriteLine("DEBUG:Begin making commands");
#endif

                DbTransaction dbTransaction = this._dbConnection.BeginTransaction();

#if DEBUG
                Console.WriteLine("DEBUG:Begin making transaction");
#endif

                command.CommandText = queryBuilder.ToString();
                command.ExecuteNonQuery();
                dbTransaction.Commit();
            }
            catch (DbException ex) // Catch any SQLiteExceptions
            {
                throw new ArgumentException("ERROR: DbException caught", ex);
            }
            finally // If package is done, close connection
            {
                this._dbConnection.Close();
#if DEBUG
                Console.WriteLine("DEBUG:Closed connection to database");
#endif
            }
        }

        /// <summary>
        /// Retreive column names
        /// </summary>
        /// <param name="tableName">Name of table to retreive columns of</param>
        /// <returns>List off columnNames</returns>
        internal List<ColumnNames> GetDbColumns(string tableName = "data")
        {
            List<ColumnNames> columnNames = new List<ColumnNames>();

            try
            {
                this._dbConnection.Open();

                string selectQuery = $"PRAGMA table_info('{tableName}')";

                DbCommand command = this._dbConnection.CreateCommand();

                DbDataReader dataReader = command.ExecuteReader();

                int cid = 0;
                string cname = string.Empty;

                while (dataReader.Read())
                {
                    for (int i = 0; i < dataReader.FieldCount; i++)
                    {
                        cid++;
                        dataReader.GetName(i);
                        ColumnNames columnName = new ColumnNames { ColumnID = cid, ColumnName = cname };
                        columnNames.Add(columnName);
                    }
#if DEBUG
                    Console.WriteLine($"INFO:{cid}. {cname}");
#endif
                }
            }
            catch (DbException ex)
            {
                throw new ArgumentException("ERROR:SQLiteException caught", ex);
            }
            finally
            {
                this._dbConnection.Close();
            }
            return columnNames;
        }

        /// <summary>
        /// Returns a list of Taxitrip Models
        /// </summary>
        /// <param name="minLat"></param>
        /// <param name="minLon"></param>
        /// <param name="maxLat"></param>
        /// <param name="maxLon"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        internal List<Models.TaxiTrip> GetData(float minLat, float minLon, float maxLat, float maxLon, string tableName = "data")
        {
            List<Models.TaxiTrip> data = new List<Models.TaxiTrip>();

            try
            {
                this._dbConnection.Open();
                string selectQuery = $"select * " +
                                     $"FROM {tableName} " +
                                     $"LIMIT 10000";

                DbCommand dbCommand = this._dbConnection.CreateCommand();
                dbCommand.CommandText = selectQuery;
                DbDataReader dataReader = dbCommand.ExecuteReader();
                
                while (dataReader.Read())
                {
                    Models.TaxiTrip taxiTrip = new Models.TaxiTrip(dataReader.GetInt32(0));
                    int pickupLonRow = 19;
                    int pickupLatRow = 18;
                    int dropoffLonRow = 22;
                    int dropoffLatRow = 21;
                    int taxiIdRow = 2;
                    int tripTotalRow = 15;

                    string check = dataReader.GetString(pickupLonRow);
                    bool missingData = true;
                    if (!string.IsNullOrEmpty(check))
                    {
                        taxiTrip.PickupCentroidLatitude = float.Parse(dataReader.GetString(pickupLatRow), CultureInfo.InvariantCulture.NumberFormat);
                        taxiTrip.PickupCentroidLongitude = float.Parse(dataReader.GetString(pickupLonRow), CultureInfo.InvariantCulture.NumberFormat);

                        check = dataReader.GetString(dropoffLatRow);

                        if (!string.IsNullOrEmpty(check))
                        {
                            string dlat = dataReader.GetString(dropoffLatRow);
                            string dlon = dataReader.GetString(dropoffLonRow);
                            taxiTrip.DropoffCentroidLatitude = float.Parse(dlat, CultureInfo.InvariantCulture.NumberFormat);
                            taxiTrip.DropoffCentroidLongitude = float.Parse(dlon, CultureInfo.InvariantCulture.NumberFormat);
                        }
                        missingData = false;
                    }

                    if (!missingData)
                    {
                        taxiTrip.TaxiId = dataReader.GetString(taxiIdRow);
                        float tripTotal;
                        if (float.TryParse(dataReader.GetString(tripTotalRow), out tripTotal))
                        {
                            taxiTrip.TripTotal = tripTotal;
                        }
                        data.Add(taxiTrip);
                    }
                }

            }
            catch(DbException ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            finally
            {
                this._dbConnection.Close();
            }

            return data;
        }
    }
}
