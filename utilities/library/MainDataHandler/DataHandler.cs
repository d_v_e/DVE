﻿using System.Collections.Generic;
using System.Data.Common;
using MainDataHandler.DBControllers;

namespace MainDataHandler
{
    /// <summary>
    /// Main class
    /// </summary>
    public class DataHandler
    {
        // Assign a field for storing the DynamicTableController
        private readonly DynamicTableController _dbTableController;


        /// <summary>
        /// Constructor of DataHandler class with extra parameters
        /// </summary>
        /// <param name="connectString">Settings used to connect to the database
        ///     SQLite format:
        ///         "Data Source=filePath; Password=password"
        ///     MySQL format:
        ///         "server=serveradress; user=user; password=password; database=database;"
        /// </param>
        /// <param name="databaseType">Thet type of database system to use</param>
        public DataHandler (DbConnection dbConnection)
        {
            // create new database table controller
            this._dbTableController = new DynamicTableController(dbConnection);
        }

        /// <summary>
        /// Get the names of columns in the database
        /// </summary>
        /// <returns></returns>
        public List<ColumnNames> GetDbColumns()
        {
            return this._dbTableController.GetDbColumns();
        }

        /// <summary>
        /// Returns data from the database based on the given parameters
        /// </summary>
        /// <param name="minLat"></param>
        /// <param name="minLon"></param>
        /// <param name="maxLat"></param>
        /// <param name="maxLon"></param>
        /// <param name="tablename"></param>
        /// <returns></returns>
        public List<Models.TaxiTrip> GetData(float minLat, float minLon, float maxLat, float maxLon, string tablename = "data")
        {
            return this._dbTableController.GetData(minLat, minLon, maxLat, maxLon, tablename);
        }
    }
}
