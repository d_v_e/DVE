package com.dve.dve3demo.network;

import com.dve.dve3demo.gui.SpawnerScreen;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.StringTextComponent;
import org.jline.utils.Log;

import java.util.Objects;

public class BlockSpawner {

    private static final BlockState blockStateAir = Blocks.AIR.getDefaultState();
    private static final BlockState blockStateVoidAir = Blocks.VOID_AIR.getDefaultState();
    // Insert a block by it's id near the player
    public static void insertBlock(Block id, Minecraft minecraft) {
        // Get current position player
        BlockPos pos = minecraft.player.getPosition();

        // Slightly alter target coordinates todo: implement facing
        int BlockPosX = pos.getX() + 3; // Horizontal: West to east
        int BlockPosY = pos.getY() + 1; // Vertical
        int BlockPosZ = pos.getZ() + 3; // Horizontal: North to South
        BlockPos targetPos = new BlockPos(BlockPosX, BlockPosY, BlockPosZ);
        Log.info("TargetPos=" + targetPos);

        // Set flag
        int flag = 11;

        // Get the minecraft world and set the blockstate to parameters
        minecraft.world.setBlockState(targetPos, id.getDefaultState(), flag);
        Log.info("DVEDEBUG: Trying to send it to the server");
        Log.info("DVEDEBUG: Inserted a block [" + id + "] at " + targetPos + " With flag:" + flag);
        minecraft.player.sendMessage(new StringTextComponent("Spawned a block (" + Objects.requireNonNull(id.getRegistryName()).getPath() + ") nearby user at " + targetPos + " With flag: " + flag));
    }

    public static void tempFixedPos(Minecraft minecraft, BlockState block, int pos_x, int pos_y, int pos_z){
        String blockStatePath = Objects.requireNonNull(block.getBlock().getRegistryName()).getPath();
        minecraft.player.sendChatMessage("/setblock " + pos_x + " " + pos_y + " " + pos_z + " " + blockStatePath);
    }

    public static void remoteFallingBlockSpawner(Minecraft minecraft, BlockState block, int pos_x, int pos_z){
        int spawnHeight = SpawnerScreen.ySpawnHeight;
        String blockStatePath = Objects.requireNonNull(block.getBlock().getRegistryName()).getPath();
        String chatMessage = "/summon falling_block " + pos_x + " " + spawnHeight + " " + pos_z + " {BlockState:{Name:'minecraft:" + blockStatePath + "'},Time:1}";
        minecraft.player.sendChatMessage(chatMessage);
    }
    public static void remoteBlockSpawnerRandomized(Minecraft minecraft, BlockState block, int pos_x, int pos_z) {
        int startHeight = SpawnerScreen.yStartHeight;
        int maxHeight = SpawnerScreen.yMaxHeight;
        String blockStatePath = Objects.requireNonNull(block.getBlock().getRegistryName()).getPath();
        // Get the block currently placed at the specified coordinates
        BlockState currentBlockState = minecraft.world.getBlockState(new BlockPos(pos_x, startHeight, pos_z));

        // If there is no block at the specified coordinate yet, place it
        if (currentBlockState == blockStateAir) {
            String chatMessage = "/setblock " + pos_x + " " + startHeight + " " + pos_z + " " + blockStatePath;
            //Log.info(chatMessage);
            minecraft.player.sendChatMessage(chatMessage);
        }

        // If there is already a block at the specified coordinate, look up until an empty spot is found
        else {
            for (int y = startHeight + 1; y < maxHeight; y++) {
                BlockState blockStateAtX = minecraft.world.getBlockState(new BlockPos(pos_x, y, pos_z)).getBlockState();
                //Log.info(blockStateAtX.toString());
                if (blockStateAtX == blockStateAir) {
                    String chatMessage = "/setblock " + pos_x + " " + y + " " + pos_z + " " + blockStatePath;
                    //Log.info(chatMessage);
                    minecraft.player.sendChatMessage(chatMessage);
                    break;
                }
            }
        }
    }

    public static void localBlockSpawner(Minecraft minecraft, BlockState block, int pos_x, int pos_z) {
        // flag sets the way the block acts
        int flag = 11;
        int startHeight = SpawnerScreen.yStartHeight;
        int maxHeight = SpawnerScreen.yMaxHeight;
        // Get the block currently placed at the specified coordinates
        BlockState currentBlockState = minecraft.world.getBlockState(new BlockPos(pos_x, startHeight, pos_z));

        // If there is already a block at the specified coordinate, look up until an empty spot is found
        if (currentBlockState != blockStateAir) {
            for (int y = startHeight + 1; y < maxHeight; y++) {
                BlockState blockStateAtX = minecraft.world.getBlockState(new BlockPos(pos_x, y, pos_z)).getBlockState();
                if (blockStateAtX != block && blockStateAtX == blockStateAir) {
                    minecraft.world.setBlockState(new BlockPos(pos_x, y, pos_z), block, flag);
                    break;
                }
            }
        }
        // If there is no block at the specified coordinate yet, place it
        else {
            minecraft.world.setBlockState(new BlockPos(pos_x, startHeight, pos_z), block, flag);
        }
    }

    // Insert BlockStates at coordinates from the database
    public static void localBlockSpawnerForWhenMapIsNotFlat(Minecraft minecraft, BlockState blockStateToPlace, BlockState blockStateToCompare, int startBlockPosX, int endBlockPosX) {
        // flag sets the way the block acts
        int flag = 11;
        int startHeight = SpawnerScreen.yStartHeight;
        int maxHeight = SpawnerScreen.yMaxHeight;
        // Get the block currently placed at the specified coordinates
        BlockState currentBlockState = minecraft.world.getBlockState(new BlockPos(startBlockPosX, startHeight, endBlockPosX));

        // If there is already a block at the specified coordinate, look up until an empty spot is found
        if (currentBlockState == blockStateToPlace || currentBlockState == blockStateToCompare) {
            for (int y = startHeight; y < (maxHeight - startHeight) + startHeight; y++) {
                BlockState blockStateAtI = minecraft.world.getBlockState(new BlockPos(startBlockPosX, y, endBlockPosX)).getBlockState();
                if (blockStateAtI != blockStateToPlace && blockStateAtI != blockStateToCompare) {
                    minecraft.world.setBlockState(new BlockPos(startBlockPosX, y, endBlockPosX), blockStateToPlace, flag);
                    break;
                }
            }
        }
        // If there is no block at the specified coordinate yet, place it
        else {
            minecraft.world.setBlockState(new BlockPos(startBlockPosX, startHeight, endBlockPosX), blockStateToPlace, flag);
        }
    }

}
