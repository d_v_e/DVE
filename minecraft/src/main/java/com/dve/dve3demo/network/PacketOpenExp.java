package com.dve.dve3demo.network;

import com.dve.dve3demo.gui.MoreScreen;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;


public class PacketOpenExp {

    public PacketOpenExp(PacketBuffer buf) {
    }

    public void toBytes(PacketBuffer buf) {
    }

    public PacketOpenExp() {
    }

    public void handle(Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(MoreScreen::open);
        ctx.get().setPacketHandled(true);
    }

}
