package com.dve.dve3demo.network;

import com.dve.dve3demo.network.Networking;
import com.dve.dve3demo.network.PacketSpawn;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.util.text.StringTextComponent;
import org.jline.utils.Log;

public class EntitySpawner {

    // Spawn an entity by it's id
    public static void spawn(String id, Minecraft minecraft) {
        // Send to the server, so the entity doens't despawn
        Networking.INSTANCE.sendToServer(new PacketSpawn(id, minecraft.player.dimension, minecraft.player.getPosition()));
        minecraft.player.sendMessage(new StringTextComponent("Spawned a " + id + " nearby user"));
        minecraft.displayGuiScreen(null);
    }

    // Use input from the user to spawn an entity
    public static void inputToSpawn(TextFieldWidget inputValue, Minecraft minecraft) {
        inputValue.setSuggestion("");
        Log.info("DVEDEBUG: input is:" + inputValue.getText());
        try {
            spawn("minecraft:" + inputValue.getText(), minecraft);
        } catch (Exception e) {
            Log.info("DVEDEBUG: That's no entity!");
        }
    }
}
