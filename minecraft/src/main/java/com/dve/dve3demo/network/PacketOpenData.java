package com.dve.dve3demo.network;

import com.dve.dve3demo.gui.DataScreen;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

public class PacketOpenData {
    public PacketOpenData(PacketBuffer buf) {
    }

    public void toBytes(PacketBuffer buf) {
    }

    public PacketOpenData() {
    }

    public void handle(Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(DataScreen::open);
        ctx.get().setPacketHandled(true);
    }
}
