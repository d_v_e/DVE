package com.dve.dve3demo.network;

import com.dve.dve3demo.DVE;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.simple.SimpleChannel;

public class Networking {

    public static SimpleChannel INSTANCE;
    private static int ID = 0;

    public static int nextID() {
        return ID++;
    }

    public static void registerMessages() {
        INSTANCE = NetworkRegistry.newSimpleChannel(new ResourceLocation(DVE.MODID, "dve3demo"), () -> "1.0", s -> true, s -> true);

        INSTANCE.registerMessage(nextID(),
                PacketOpenGui.class,
                PacketOpenGui::toBytes,
                PacketOpenGui::new,
                PacketOpenGui::handle);
        INSTANCE.registerMessage(nextID(),
                PacketSpawn.class,
                PacketSpawn::toBytes,
                PacketSpawn::new,
                PacketSpawn::handle);
        INSTANCE.registerMessage(nextID(),
                PacketOpenExp.class,
                PacketOpenExp::toBytes,
                PacketOpenExp::new,
                PacketOpenExp::handle);
        INSTANCE.registerMessage(nextID(),
                PacketOpenData.class,
                PacketOpenData::toBytes,
                PacketOpenData::new,
                PacketOpenData::handle);
    }
}
