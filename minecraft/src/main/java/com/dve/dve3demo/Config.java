package com.dve.dve3demo;

import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import com.electronwill.nightconfig.core.io.WritingMode;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.common.Mod;

import java.nio.file.Path;

@Mod.EventBusSubscriber
public class Config {

    public static final String CATEGORY_GENERAL = "general";
    public static final String CATEGORY_POWER = "power";
    public static final String CATEGORY_PATH = "path";
    public static final String SUBCATEGORY_PATHS = "filepath";


    private static final ForgeConfigSpec.Builder COMMON_BUILDER = new ForgeConfigSpec.Builder();
    private static final ForgeConfigSpec.Builder CLIENT_BUILDER = new ForgeConfigSpec.Builder();

    public static final ForgeConfigSpec COMMON_CONFIG;
    public static final ForgeConfigSpec CLIENT_CONFIG;

    public static ForgeConfigSpec.ConfigValue<String> PRE_PATH;
    public static ForgeConfigSpec.ConfigValue<String> FILE_PATH;


    static {
        COMMON_BUILDER.comment("General settings").push(CATEGORY_GENERAL);
        COMMON_BUILDER.pop();

        COMMON_BUILDER.comment("Power settings").push(CATEGORY_POWER);
        COMMON_BUILDER.pop();

        COMMON_BUILDER.comment("Filepath settings").push(CATEGORY_PATH);
        setupPathsConfig();
        COMMON_BUILDER.pop();

        COMMON_CONFIG = COMMON_BUILDER.build();
        CLIENT_CONFIG = CLIENT_BUILDER.build();
    }

    private static void setupPathsConfig() {
        COMMON_BUILDER.comment("Filepath settings").push(SUBCATEGORY_PATHS);
        PRE_PATH = COMMON_BUILDER.comment("Path to add before the actual filepath").define("prepath", "jdbc:sqlite:");
        FILE_PATH = COMMON_BUILDER.comment("Path of database file").define("filepath",
                "[FILEPATHHERE]");

        COMMON_BUILDER.pop();
    }

    public static void loadConfig(ForgeConfigSpec spec, Path path) {

        final CommentedFileConfig configData = CommentedFileConfig.builder(path)
                .sync()
                .autosave()
                .writingMode(WritingMode.REPLACE)
                .build();

        configData.load();
        spec.setConfig(configData);
    }


}
