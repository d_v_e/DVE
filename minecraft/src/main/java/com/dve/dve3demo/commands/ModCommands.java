package com.dve.dve3demo.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.tree.LiteralCommandNode;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;

public class ModCommands {

    public static void register(CommandDispatcher<CommandSource> dispatcher) {
        LiteralCommandNode<CommandSource> cmdDve = dispatcher.register(
                Commands.literal("dve")
                        .then(CommandSpawner.register(dispatcher))
                        .then(CommandMore.register(dispatcher))
                        .then(CommandLoad.register(dispatcher))
        );

        dispatcher.register(Commands.literal("dve3demo").redirect(cmdDve));
    }

}
