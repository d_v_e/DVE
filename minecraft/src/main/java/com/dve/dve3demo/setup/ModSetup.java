package com.dve.dve3demo.setup;

import com.dve.dve3demo.blocks.ModBlocks;
import com.dve.dve3demo.network.Networking;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;

public class ModSetup {

    public final ItemGroup itemGroup = new ItemGroup("dve3demo") {
        @Override
        public ItemStack createIcon() {
            return new ItemStack(ModBlocks.DVEBLOCK);
        }
    };

    public void init() {
        MinecraftForge.EVENT_BUS.register(new ForgeEventHandlers());
        Networking.registerMessages();
    }

}
