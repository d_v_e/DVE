package com.dve.dve3demo.blocks;

import net.minecraftforge.registries.ObjectHolder;

public class ModBlocks {
    @ObjectHolder("dve3demo:dveblock")
    public static DveBlock DVEBLOCK;

    @ObjectHolder("dve3demo:taxiaffiliationservicesblock")
    public static TaxiAffiliationServicesBlock TAXIAFFILIATIONSERVICESBLOCK;

    @ObjectHolder("dve3demo:blueribbonblock")
    public static BlueRibbonBlock BLUERIBBONBLOCK;

    @ObjectHolder("dve3demo:cityserviceblock")
    public static CityServiceBlock CITYSERVICEBLOCK;

    @ObjectHolder("dve3demo:flashcabblock")
    public static FlashCabBlock FLASHCABBLOCK;
}
