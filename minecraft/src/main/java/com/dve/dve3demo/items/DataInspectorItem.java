package com.dve.dve3demo.items;

import com.dve.dve3demo.DVE;
import com.dve.dve3demo.database.DatabaseHandler;
import com.dve.dve3demo.gui.DataScreen;
import com.dve.dve3demo.gui.SpawnerScreen;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.item.Item;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.jline.utils.Log;

import java.util.Map;
import java.util.Objects;

@Mod.EventBusSubscriber
public class DataInspectorItem extends Item{
    // Set the properties of the Data Inspector Item
    public DataInspectorItem() {
        super(new Item.Properties()
                .maxStackSize(1)
                .group(DVE.setup.itemGroup));
        setRegistryName("datainspectoritem");
    }

    // Initialize a long for the latest time, 404: Funny not found
    public static long latestTime = 404;
    public static Map<String, Object> dataFromDatabase;
    public static BlockPos aroundOtherBlock1;
    public static BlockPos aroundOtherBlock2;
    public static BlockPos aroundOtherBlock3;
    public static BlockPos aroundOtherBlock4;

    // Subscribe to the PlayerInteractEvent.RightClickBlock event
    @SubscribeEvent(priority = EventPriority.NORMAL, receiveCanceled = true)
    public static void handleCropRightClick(final PlayerInteractEvent.RightClickBlock event) {
        // If the user clicked with DIT, inspect the block
        if (event.getItemStack().getDisplayName().getString().equals("Data Inspector Tool")) {
            Log.info("Clicked something with DIT");
            long timeNow = System.currentTimeMillis(); // get current time in milliseconds

            // If the difference is too small, it means the event got fired multiple times within the same click
            // If this is the case, return and ignore the 'click'
            Log.info("DVE: latestTime = " + latestTime + ", timeNow = " + timeNow);
            if (timeNow - latestTime < 2000 && latestTime != 404) {
                Log.info("User clicked multiple times within 2000 millis, ignoring..");
                return;
            }

            // If the click is unique/real, get the blockpos and the block clicked
            else {
                BlockPos clickedPos = event.getPos();
                int clickedPosX = clickedPos.getX();
                int clickedPosY = clickedPos.getY();
                int clickedPosZ = clickedPos.getZ();
                Block clickedBlock = event.getWorld().getBlockState(event.getPos()).getBlock();
                String clickedBlockName = Objects.requireNonNull(clickedBlock.getRegistryName()).getPath();
                int foreign_key = 0;
                int other_x = 0;
                int other_z = 0;
                boolean isStart;
                int blocksBelowFromDifferentType = 0;

                if (SpawnerScreen.blocksUsedForStart.contains(clickedBlockName)){
                    Log.info("This is a block used for START");
                    isStart = true;

                }
                else if (SpawnerScreen.blocksUsedForEnd.contains(clickedBlockName)){
                    Log.info("This is a block used for END");
                    isStart = false;
                }
                else {
                    Log.warn("Can't get the data of this block because it wasn't used for a visualisation with custom blocks");
                    Minecraft.getInstance().player.sendChatMessage("Can't get the data of this block because it wasn't used for a visualisation with custom blocks");
                    return;
                }

                // Since DVE doesn't spawn grass blocks, these presumed miss clicks can be ignored
                if (clickedBlock == Blocks.GRASS_BLOCK) {
                    Minecraft.getInstance().player.sendChatMessage("You clicked on a " + clickedBlockName +
                            ". These type of default blocks do not contain any data.");
                    return;
                }

                // If it's a real click on a block that could contain data, check the database
                // if the block was visualised, if yes, try to
                Minecraft.getInstance().player.sendChatMessage("You clicked on a " + clickedBlockName +
                        " at " + clickedPosX + ", " + clickedPosY + ", " + clickedPosZ + ", checking for data..");
                //DatabaseHandler.getOtherBlockFromDatabase(SpawnerScreen.rowBatchSize, clickedPos.getX(), clickedPos.getY(), clickedPos.getZ());

                if (isStart) {
                    World world = Minecraft.getInstance().world;
                    for (int i = SpawnerScreen.yStartHeight; i < clickedPosY; i++) {
                        String blockNameAtX = Objects.requireNonNull(
                                world.getBlockState(new BlockPos(clickedPosX, i, clickedPosZ)).getBlock().getRegistryName()).getPath();
                        if (SpawnerScreen.blocksUsedForEnd.contains(blockNameAtX)){
                            blocksBelowFromDifferentType++;
                        }
                    }
                }
                else {
                    World world = Minecraft.getInstance().world;
                    for (int i = SpawnerScreen.yStartHeight; i < clickedPosY; i++) {
                        String blockNameAtX = Objects.requireNonNull(
                                world.getBlockState(new BlockPos(clickedPosX, i, clickedPosZ)).getBlock().getRegistryName()).getPath();
                        if (SpawnerScreen.blocksUsedForStart.contains(blockNameAtX)){
                            blocksBelowFromDifferentType++;
                        }
                    }
                }
                Log.info("This is blocksBelowFromDifferentType" + blocksBelowFromDifferentType);


                // Get the necessary data from the minecraft view
                // TODO Possibly make dataFromView <String, long> to prevent overflow issues
                Map<String, Integer> dataFromView = DatabaseHandler.getDataFromView(SpawnerScreen.rowBatchSize, clickedPosX, clickedPosY - blocksBelowFromDifferentType, clickedPosZ, isStart);
                assert dataFromView != null;
                if (dataFromView.isEmpty()){
                    Log.warn("DVE: Could not find any data");
                    Minecraft.getInstance().player.sendChatMessage("Error: Could not find any data on this block");
                }
                else {
                    for (Map.Entry<String, Integer> entry : dataFromView.entrySet()) {
                        //Minecraft.getInstance().player.sendChatMessage(entry.getKey() + " = " + entry.getValue());
                        if (entry.getKey().equals("id")) {
                            foreign_key = entry.getValue();
                            Minecraft.getInstance().player.sendChatMessage("id = : " + foreign_key);
                        }
                        if (isStart) {
                            if (entry.getKey().equals("end_x")) {
                                other_x = entry.getValue();
                            }
                            if (entry.getKey().equals("end_z")) {
                                other_z = entry.getValue();
                            }
                        }
                        else {
                            if (entry.getKey().equals("start_x")) {
                                other_x = entry.getValue();
                            }
                            if (entry.getKey().equals("start_z")) {
                                other_z = entry.getValue();
                            }
                        }
                    }

                    // Get all coordinates at those coordinates
                    int amountAtX1 = DatabaseHandler.getAmountOfBlocksAtX(other_x, other_z, foreign_key, isStart);
                    int amountAtX2 = DatabaseHandler.getAmountOfBlocksAtX(other_x, other_z, foreign_key, !isStart);
                    int totalAmount = amountAtX1 + amountAtX2 + SpawnerScreen.yStartHeight;
                    Log.info("DVE: amountAtX1: " + amountAtX1);
                    Log.info("DVE: amountAtX2: " + amountAtX2);
                    Log.info("DVE: totalAmount: " + totalAmount);
                    Log.info("This should mean that the other block is over at:" + other_x + "  "  + totalAmount + " " + other_z);
                    Minecraft.getInstance().player.sendChatMessage("This means that the other block is over at:" + other_x + "  "  + totalAmount + " " + other_z + " Should be highlighted with glowstones.");

                    World world = Minecraft.getInstance().world;
                    if (!(aroundOtherBlock1 == null)){
                        world.setBlockState(aroundOtherBlock1, Blocks.AIR.getDefaultState());
                        world.setBlockState(aroundOtherBlock2, Blocks.AIR.getDefaultState());
                        world.setBlockState(aroundOtherBlock3, Blocks.AIR.getDefaultState());
                        world.setBlockState(aroundOtherBlock4, Blocks.AIR.getDefaultState());
                    }

                    aroundOtherBlock1 = new BlockPos(other_x + 1, totalAmount, other_z + 1);
                    aroundOtherBlock2 = new BlockPos(other_x + 1, totalAmount, other_z - 1);
                    aroundOtherBlock3 = new BlockPos(other_x - 1, totalAmount, other_z + 1);
                    aroundOtherBlock4 = new BlockPos(other_x - 1, totalAmount, other_z - 1);

                    world.setBlockState(aroundOtherBlock1, Blocks.GLOWSTONE.getDefaultState());
                    world.setBlockState(aroundOtherBlock2, Blocks.GLOWSTONE.getDefaultState());
                    world.setBlockState(aroundOtherBlock3, Blocks.GLOWSTONE.getDefaultState());
                    world.setBlockState(aroundOtherBlock4, Blocks.GLOWSTONE.getDefaultState());

                    // Count which one is the one with the ID

                    Log.info("DVE: Trying to get the other data..");
                    dataFromDatabase = DatabaseHandler.getDataById(foreign_key);
                    assert dataFromDatabase != null;
                    if (dataFromDatabase.isEmpty()) {
                        Log.warn("DVE: Could not find any data because the ID is missing");
                        Minecraft.getInstance().player.sendChatMessage("Error: Could not find any data on this block");
                    } else {
                        Log.info("DVE: Looping through data");
                        for (Map.Entry<String, Object> entry : dataFromDatabase.entrySet()) {
                            Minecraft.getInstance().player.sendChatMessage(entry.getKey() + " = " + entry.getValue().toString());
                            DataScreen.open();
                        }
                    }

                }

            }
            latestTime = timeNow;
        }

    }
}
