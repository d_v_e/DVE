package com.dve.dve3demo.items;

import net.minecraft.item.Item;

public class DVE extends Item {
    public DVE() {
        super(new Item.Properties()
                .maxStackSize(1)
                .group(com.dve.dve3demo.DVE.setup.itemGroup));
        setRegistryName("dve");
    }
}

