package com.dve.dve3demo.gui;

import com.dve.dve3demo.DVE;
import com.dve.dve3demo.network.BlockSpawner;
import com.dve.dve3demo.network.EntitySpawner;
import com.google.common.base.Preconditions;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.command.arguments.BlockStateArgument;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;
import org.jline.utils.Log;

import java.util.Objects;

public class MoreScreen extends Screen {

    // Set default sizes in pixels
    private static final int WIDTH = 350;
    private static final int HEIGHT = 200;
    // Alternative to database: keep a list of all blockPos
    // Initialize a new ArrayList to store blockPos' in (only for reading from an XML)
    // private ArrayList<BlockPos> blockPosList = new ArrayList<>();

    // Initialize a new ResourceLocation for the GUI with a specified background
    private final ResourceLocation GUI = new ResourceLocation(DVE.MODID, "textures/gui/spawner_gui_new3.png");

    // Initialize the defaultBlock, the Block(State) that will be used to visualise everything
    // Will be overwritten if a user specifies a new defaultBlock
    private static BlockState defaultBlockStateStart = Blocks.RED_WOOL.getDefaultState();
    private static BlockState defaultBlockStateEnd = Blocks.GREEN_WOOL.getDefaultState();

    // Made private! - Set a textComponent to explain what this GUI does
    private MoreScreen() {
        super(new StringTextComponent("Spawn something"));
    }

    @Override
    protected void init() {

        // Calculate the relative values; the middle of the GUI
        int relX = (this.width - WIDTH) / 2;
        int relY = (this.height - HEIGHT) / 2;

        // Set the FontRenderer to the one used by the ingame GUI
        FontRenderer fontRenderer = getMinecraft().ingameGUI.getFontRenderer();

        // Place a new textfield where the user can type the name of an entity
        // https://www.digminecraft.com/lists/entity_list_pc.php
        // Alternative buttons
        TextFieldWidget entityInputTxt = new TextFieldWidget(
                fontRenderer,
                relX + 10,
                relY + 10,
                76,
                20,
                "Name of an entity, e.g.: cow'"
        );
        entityInputTxt.setSuggestion("cow");
        entityInputTxt.setTextColor(16755200);
        entityInputTxt.setFGColor(11141120);
        entityInputTxt.setMaxStringLength(15);
        addButton(entityInputTxt);

        // Use input from inputTextfield2 to spawn entity
        addButton(new Button(
                relX + 92,
                relY + 10,
                78,
                20,
                "Spawn entity",
                button -> EntitySpawner.inputToSpawn(entityInputTxt, minecraft)
        ));

        TextFieldWidget blockInputTxt = new TextFieldWidget(
                fontRenderer,
                relX + 10,
                relY + 37,
                76,
                20,
                "Name of an entity, e.g.: 'glowstone'"
        );
        blockInputTxt.setSuggestion("glowstone");
        blockInputTxt.setTextColor(16755200);
        blockInputTxt.setFGColor(11141120);
        blockInputTxt.setMaxStringLength(15);
        addButton(blockInputTxt);

        addButton(new Button(
                relX + 92,
                relY + 37,
                78,
                20,
                "Insert block",
                button -> {
                    assert minecraft != null;
                    BlockSpawner.insertBlock(Objects.requireNonNull(getStateFromString(blockInputTxt.getText())).getBlock(), minecraft);
                }));

        addButton(new Button(
                relX + 182,
                relY + 10,
                160,
                20,
                "Insert a glowstone",
                button -> {
                    assert minecraft != null;
                    BlockSpawner.insertBlock(Blocks.GLOWSTONE, minecraft);
                }));
        addButton(new Button(
                relX + 182,
                relY + 37,
                160,
                20,
                "Insert cow",
                button -> {
                    assert minecraft != null;
                    EntitySpawner.spawn("cow", minecraft);
                }));

        addButton(new Button(
                relX + 182,
                relY + 64,
                160,
                20,
                "Insert villager",
                button -> {
                    assert minecraft != null;
                    EntitySpawner.spawn("villager", minecraft);
                }));

        addButton(new Button(
                relX + 182,
                relY + 91,
                160,
                20,
                "Insert Andesite (-> pig)",
                button -> {
                    assert minecraft != null;
                    EntitySpawner.spawn("polished_andesite", minecraft);
                }));
        // gives pig because it's an block, where entity was expected

        addButton(new Button(
                relX + 182,
                relY + 118,
                160,
                20,
                "Teleport to center",
                button -> Minecraft.getInstance().player.sendChatMessage("/teleport -116852 40 55870")
        ));

        addButton(new Button(
                relX + 182,
                relY + 145,
                160,
                20,
                "Remove all entities",
                button -> {
                    Minecraft.getInstance().player.sendChatMessage("/difficulty peaceful");
                    Minecraft.getInstance().player.sendChatMessage("/kill @e[type=!player]");
                    Minecraft.getInstance().player.sendChatMessage("/gamerule doMobSpawning false");
                }
        ));
    }

    private static BlockState getStateFromString(String inputString) {
        Preconditions.checkNotNull(inputString, "String to parse must not be null");
        try {
            // Get the blockState by parsing the input string
            return new BlockStateArgument().parse(new StringReader("minecraft:"+inputString)).getState();
        } catch (final CommandSyntaxException e) {
            Log.warn("DVEDEBUG: Failed to parse blockstate" + inputString + "!");
        }
        return null;
    }

    // Pause the game (default false)
    @Override
    public boolean isPauseScreen() {
        return false;
    }

    // Render the GUI
    @Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        Objects.requireNonNull(this.minecraft).getTextureManager().bindTexture(GUI);
        int relX = (this.width - WIDTH) / 2;
        int relY = (this.height - HEIGHT) / 2;
        this.blit(relX, relY, 0, 0, WIDTH, HEIGHT);
        super.render(mouseX, mouseY, partialTicks);
    }

    // Open the GUI
    public static void open() {
        Minecraft.getInstance().displayGuiScreen(new MoreScreen());
    }
}
