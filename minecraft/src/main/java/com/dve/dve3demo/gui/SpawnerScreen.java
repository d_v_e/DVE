package com.dve.dve3demo.gui;

import com.dve.dve3demo.DVE;
import com.dve.dve3demo.util.HeatmapSpawner;
import com.google.common.base.Preconditions;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.command.arguments.BlockStateArgument;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;
import org.jline.utils.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SpawnerScreen extends Screen {

    // Set default sizes in pixels
    private static final int WIDTH = 350;
    private static final int HEIGHT = 200;
    // Alternative to database: keep a list of all blockPos
    // Initialize a new ArrayList to store blockPos' in (only for reading from an XML)
    // private ArrayList<BlockPos> blockPosList = new ArrayList<>();

    // Initialize a new ResourceLocation for the GUI with a specified background
    private final ResourceLocation GUI = new ResourceLocation(DVE.MODID, "textures/gui/spawner_gui_new3.png");

    // Initialize the defaultBlock, the Block(State) that will be used to visualise everything
    // Will be overwritten if a user specifies a new defaultBlock
    public static BlockState blockStateStart = Blocks.RED_WOOL.getDefaultState();
    public static BlockState blockStateEnd = Blocks.GREEN_WOOL.getDefaultState();

    // Keep track on which blocks have been used for either start or end
    public static List<String> blocksUsedForStart = new ArrayList<>();
    public static List<String> blocksUsedForEnd = new ArrayList<>();

    public static int rowBatchSize = 100;
    public static int rowsVisualised = 0;
    public static int stopAtXBatches = 1;
    public static int yStartHeight = 4;
    public static int yMaxHeight = 30;
    public static int ySpawnHeight = 30;
    public static int newHeightFrequency = 1;
    public static int changeHeightBy = 3;

    // Made private! - Set a textComponent to explain what this GUI does
    private SpawnerScreen() {
        super(new StringTextComponent("Spawn something"));
    }

    @Override
    protected void init() {

        // Calculate the relative values; the middle of the GUI
        int relX = (this.width - WIDTH) / 2;
        int relY = (this.height - HEIGHT) / 2;

        // Set the FontRenderer to the one used by the ingame GUI
        FontRenderer fontRenderer = getMinecraft().ingameGUI.getFontRenderer();

        // Place a new textfield at the top where users can choose the defaultBlock,
        // This is the block that will be used for visualizations.
        TextFieldWidget inputTextfield0 = new TextFieldWidget(
                fontRenderer,
                relX + 10,
                relY + 10,
                76,
                20,
                "Limit of rows to visualise"
        );
        inputTextfield0.setSuggestion("100");
        inputTextfield0.setTextColor(16755200); // set the textcolor to a decimal number (https://convertingcolors.com/decimal-color-16755200.html)
        inputTextfield0.setFGColor(11141120); // set the background color
        inputTextfield0.setMaxStringLength(9); // set the max length of the input // max: 2.147.483.647
        addButton(inputTextfield0); // Add the 'button' (which is a textbox)

        // Place a new button that sets the default block
        addButton(new Button(
                relX + 92,
                relY + 10,
                78,
                20,
                "Set batch size",
                button -> rowBatchSize = Integer.parseInt(inputTextfield0.getText())
        ));

        // Place a new textfield at the top where users can choose the defaultBlock,
        // This is the block that will be used for visualizations.
        TextFieldWidget inputTextfield = new TextFieldWidget(
                fontRenderer,
                relX + 10,
                relY + 37,
                76,
                20,
                "Name of block, e.g.: glowstone"
        );

        inputTextfield.setSuggestion(Objects.requireNonNull(blockStateStart.getBlock().getRegistryName()).getPath());
        inputTextfield.setTextColor(16755200); // set the textcolor to a decimal number (https://convertingcolors.com/decimal-color-16755200.html)
        inputTextfield.setFGColor(11141120); // set the background color
        inputTextfield.setMaxStringLength(15); // set the max length of the input
        addButton(inputTextfield); // Add the 'button' (which is a textbox)

        // Place a new button that sets the default block
        addButton(new Button(
                relX + 92,
                relY + 37,
                78,
                20,
                "Set block 'A'",
                button -> blockStateStart = getStateFromString(inputTextfield.getText())
        ));

        TextFieldWidget inputTextfield3 = new TextFieldWidget(
                fontRenderer,
                relX + 10,
                relY + 64,
                76,
                20,
                "Name of block, e.g.: blue_wool"
        );
        inputTextfield3.setSuggestion(Objects.requireNonNull(blockStateEnd.getBlock().getRegistryName()).getPath());
        inputTextfield3.setTextColor(16755200); // set the textcolor to a decimal number (https://convertingcolors.com/decimal-color-16755200.html)
        inputTextfield3.setFGColor(11141120); // set the background color
        inputTextfield.setMaxStringLength(15); // set the max length of the input
        addButton(inputTextfield3); // Add the 'button' (which is a textbox)

        // Place a new button that sets the default block
        addButton(new Button(
                relX + 92,
                relY + 64,
                78,
                20,
                "Set block 'B'",
                button -> blockStateEnd = getStateFromString(inputTextfield3.getText())
        ));

        TextFieldWidget inputTextfield4 = new TextFieldWidget(
                fontRenderer,
                relX + 10,
                relY + 91,
                76,
                20,
                "Set the start height (default=4)"
        );
        inputTextfield4.setSuggestion("4");
        inputTextfield4.setTextColor(16755200); // set the textcolor to a decimal number (https://convertingcolors.com/decimal-color-16755200.html)
        inputTextfield4.setFGColor(11141120); // set the background color
        inputTextfield4.setMaxStringLength(3); // set the max length of the input // max: 2.147.483.647
        addButton(inputTextfield4); // Add the 'button' (which is a textbox)

        // Place a new button that sets the default block
        addButton(new Button(
                relX + 92,
                relY + 91,
                78,
                20,
                "Set start Y",
                button -> yStartHeight = Integer.parseInt(inputTextfield4.getText())
        ));

        TextFieldWidget maxHeightInputTxt = new TextFieldWidget(
                fontRenderer,
                relX + 10,
                relY + 118,
                76,
                20,
                "Set spawn height (max=256)"
        );
        maxHeightInputTxt.setSuggestion("30");
        maxHeightInputTxt.setTextColor(16755200); // set the textcolor to a decimal number (https://convertingcolors.com/decimal-color-16755200.html)
        maxHeightInputTxt.setFGColor(11141120); // set the background color
        maxHeightInputTxt.setMaxStringLength(3); // set the max length of the input // max: 2.147.483.647
        addButton(maxHeightInputTxt); // Add the 'button' (which is a textbox)

        // Place a new button that sets the max Y block
        addButton(new Button(
                relX + 92,
                relY + 118,
                78,
                20,
                "Set spawn Y",
                button -> {
                    ySpawnHeight = Integer.parseInt(maxHeightInputTxt.getText());
                    yMaxHeight = Integer.parseInt(maxHeightInputTxt.getText());
                }
        ));

        TextFieldWidget freqNewHeightInputTxt = new TextFieldWidget(
                fontRenderer,
                relX + 10,
                relY + 145,
                76,
                20,
                "Set the frequency of after how many blocks, the height should increase"
        );
        freqNewHeightInputTxt.setSuggestion("1");
        freqNewHeightInputTxt.setTextColor(16755200); // set the textcolor to a decimal number (https://convertingcolors.com/decimal-color-16755200.html)
        freqNewHeightInputTxt.setFGColor(11141120); // set the background color
        freqNewHeightInputTxt.setMaxStringLength(6); // set the max length of the input // max: 2.147.483.647
        addButton(freqNewHeightInputTxt); // Add the 'button' (which is a textbox)

        // Place a new button that sets the max Y block
        addButton(new Button(
                relX + 92,
                relY + 145,
                78,
                20,
                "Set freq dY",
                button -> newHeightFrequency = Integer.parseInt(freqNewHeightInputTxt.getText())
        ));

        TextFieldWidget changeHeightByInputTxt = new TextFieldWidget(
                fontRenderer,
                relX + 10,
                relY + 172,
                76,
                20,
                "Set the amount of blocks to higher every time"
        );
        changeHeightByInputTxt.setSuggestion("3");
        changeHeightByInputTxt.setTextColor(16755200); // set the textcolor to a decimal number (https://convertingcolors.com/decimal-color-16755200.html)
        changeHeightByInputTxt.setFGColor(11141120); // set the background color
        changeHeightByInputTxt.setMaxStringLength(3); // set the max length of the input // max: 2.147.483.647
        addButton(changeHeightByInputTxt); // Add the 'button' (which is a textbox)

        addButton(new Button(
                relX + 92,
                relY + 172,
                78,
                20,
                "Set dY amount",
                button -> changeHeightBy = Integer.parseInt(changeHeightByInputTxt.getText())
        ));

        addButton(new Button(
                relX + 182,
                relY + 10,
                160,
                20,
                "Visualise static",
                button -> HeatmapSpawner.loadHeatmap(minecraft, blockStateStart, blockStateEnd, rowBatchSize, stopAtXBatches, false, false)
        ));

        addButton(new Button(
                relX + 182,
                relY + 37,
                160,
                20,
                "Visualise randomized static",
                button -> HeatmapSpawner.loadHeatmap(minecraft, blockStateStart, blockStateEnd, rowBatchSize, stopAtXBatches, true, false)
        ));

        addButton(new Button(
                relX + 182,
                relY + 64,
                160,
                20,
                "Visualise custom falling blocks",
                button -> {
                    String chatMessage = "/dve load";
                    Minecraft.getInstance().player.sendChatMessage(chatMessage);
                }
        ));
    }

    private static BlockState getStateFromString(String inputString) {
        Preconditions.checkNotNull(inputString, "String to parse must not be null");
        try {
            // Get the blockState by parsing the input string
            return new BlockStateArgument().parse(new StringReader("minecraft:"+inputString)).getState();
        } catch (final CommandSyntaxException e) {
            Log.warn("DVEDEBUG: Failed to parse blockstate" + inputString + "!");
        }
        return null;
    }

    // Pause the game (default false)
    @Override
    public boolean isPauseScreen() {
        return false;
    }

    // Render the GUI
    @Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        Objects.requireNonNull(this.minecraft).getTextureManager().bindTexture(GUI);
        int relX = (this.width - WIDTH) / 2;
        int relY = (this.height - HEIGHT) / 2;
        this.blit(relX, relY, 0, 0, WIDTH, HEIGHT);
        super.render(mouseX, mouseY, partialTicks);
    }

    // Open the GUI
    public static void open() {
        Minecraft.getInstance().displayGuiScreen(new SpawnerScreen());
    }
}
