﻿using UnityEngine;
using MainDataHandler;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System;

public class CoordinateTranslator: MonoBehaviour
{
    public GameObject sprite;
    public GameObject mainCamera;

    void Start()
    {
        MySqlConnection mySqlConnection = new MySqlConnection("Server = server; Port = 3306; Database = dve_dev; user = dve_dev; Password = password");
        DataHandler dataHandler = new DataHandler(mySqlConnection);
        List<MainDataHandler.Models.TaxiTrip> taxiTrips;
        Console.WriteLine("we gaan data ophalen");
        //taxiTrips = dataHandler.GetData(0f, 0f, 0f, 0f, "testdata");
        taxiTrips = new List<MainDataHandler.Models.TaxiTrip>();
        float maxX = 0f;
        float maxY = 0f;
        float minX = 5000f;
        float minY = 0f;
        bool firstRun = true;
        Console.WriteLine("We hebben data opgehaald");  

        foreach (MainDataHandler.Models.TaxiTrip taxiTrip in taxiTrips)
        {
            GameObject dataPointBegin = Instantiate(sprite);
            GameObject dataPointEnd = Instantiate(sprite);
            dataPointBegin.GetComponent<SpriteRenderer>().color = Color.green;
            dataPointEnd.GetComponent<SpriteRenderer>().color = Color.red;
            dataPointBegin.transform.localScale = new Vector2(2f, 2f);
            dataPointEnd.transform.localScale = new Vector2(1f, 1f);
            float pickupX = taxiTrip.PickupCentroidLongitude * 100f;
            float pickupY = taxiTrip.PickupCentroidLatitude * 100f;
            float dropoffX = taxiTrip.DropoffCentroidLongitude * 100f;
            float dropoffY = taxiTrip.DropoffCentroidLatitude * 100f;

            if(pickupX > maxX || firstRun)
            {
                maxX = pickupX;
            }

            if(pickupY < maxY || firstRun)
            {
                maxY = pickupY;
            }

            if (pickupX < minX || firstRun)
            {
                minX = pickupX;
            }

            if (pickupY > minY || firstRun)
            {
                minY = pickupY;
            }

            firstRun = false;
            
            dataPointBegin.transform.localPosition = new Vector2(pickupX , pickupY);
            dataPointEnd.transform.localPosition = new Vector2(dropoffX, dropoffY);   
        }
        float cameraScaleX = maxX - minX;
        float cameraScaleY = maxY - minY;
        cameraScaleY = -cameraScaleY;
        mainCamera.transform.position = new Vector3(minX + (cameraScaleX / 2), minY - (cameraScaleY / 2), -40f);
        mainCamera.transform.localScale = new Vector2(cameraScaleX, cameraScaleY);
    }

    void Update()
    {

    }
}