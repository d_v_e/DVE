﻿using UnityEngine;
using UnityEngine.UI;
using Crosstales.FB;
using MainDataHandler;
using MySql.Data.MySqlClient;

public class SelectFile : MonoBehaviour
{
    private readonly string[] extension = { "XML" };
    private string folderPath;
    private string xmlPath;
    public GameObject InputDatabaseName;
    public GameObject InputDatabasePassword;

    /// <summary>
    /// When this method is called, a popup will be shown, to select a path.
    /// </summary>
    public void SelectPath()
    {
        folderPath = FileBrowser.OpenSingleFolder();
    }

    /// <summary>
    /// When this method is called, a filebrowser will show.
    /// </summary>
    public void SelectXML()
    {
        xmlPath = FileBrowser.OpenSingleFile("Select your XML dataset", "", extension);
    }

    public GameObject gameObject;

    /// <summary>
    /// 
    /// </summary>
    public void EditVariables()
    {
        var panel = GameObject.Find("UIScreenGroup");

        GameObject a = (GameObject)Instantiate(gameObject);
        a.transform.SetParent(panel.transform, false);
        a.name = "test";
        a.SetActive(true);
    }

    /// <summary>
    /// 
    /// </summary>
    public void CreateDatabase()
    {
        string databaseName = InputDatabaseName.GetComponent<Text>().text + ".sqlite3";
        string databasePassword = InputDatabasePassword.GetComponent<Text>().text;

        MySqlConnection mysqlConnection = new MySqlConnection("Server = server; Port = 3306; Database = dve_dev; user = dve_dev; Password = password");

        XmlConverter bigXmlReader = new XmlConverter(xmlPath, mysqlConnection, databaseName);
        bigXmlReader.CreateDatabase();
        bigXmlReader.WriteXmlToDatabase();

        //ToDo: Make a progress bar

        //ToDo: After the data is imported, show new dialog.
    }
}
