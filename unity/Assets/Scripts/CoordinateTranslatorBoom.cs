﻿using UnityEngine;
using MainDataHandler;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System;

public class CoordinateTranslatorBoom : MonoBehaviour
{
    public GameObject sprite;
    public GameObject mainCamera;

    void Start()
    {
        MySqlConnection mySqlConnection = new MySqlConnection("Server = server; Port = 3306; Database = dve_dev; user = dve_dev; Password = password");
        DataHandler dataHandler = new DataHandler(mySqlConnection);
        Console.WriteLine("We gaan data ophalen");
        List<MainDataHandler.Models.TaxiTrip> taxiTrips;
        taxiTrips = dataHandler.GetData(0f, 0f, 0f, 0f, "testdata");
        float maxX = 0f;
        float maxY = 0f;
        float minX = 5000f;
        float minY = 0f;
        bool firstRun = true;
        Console.WriteLine("We hebben data opgehaald");

        foreach (MainDataHandler.Models.TaxiTrip taxiTrip in taxiTrips)
        {
            GameObject dataPointBegin = Instantiate(sprite);
            GameObject dataPointEnd = Instantiate(sprite);
            dataPointBegin.GetComponent<Renderer>().material.color = Color.green;
            dataPointEnd.GetComponent<Renderer>().material.color = Color.red;
            dataPointBegin.transform.localScale = new Vector3(3f, 3f, 3f);
            dataPointEnd.transform.localScale = new Vector3(3f, 3f, 3f);
            float pickupX = taxiTrip.PickupCentroidLongitude * 100f;
            float pickupY = taxiTrip.PickupCentroidLatitude * 100f;
            float dropoffX = taxiTrip.DropoffCentroidLongitude * 100f;
            float dropoffY = taxiTrip.DropoffCentroidLatitude * 100f;

            if (pickupX > maxX || firstRun)
            {
                maxX = pickupX;
            }

            if (pickupY < maxY || firstRun)
            {
                maxY = pickupY;
            }

            if (pickupX < minX || firstRun)
            {
                minX = pickupX;
            }

            if (pickupY > minY || firstRun)
            {
                minY = pickupY;
            }

            firstRun = false;

            dataPointBegin.transform.localPosition = new Vector3(dropoffX, 0f, pickupY);
            dataPointEnd.transform.localPosition = new Vector3(dropoffX, 0f, dropoffY);
        }
        float cameraScaleX = maxX - minX;
        float cameraScaleY = maxY - minY;
        cameraScaleY = -cameraScaleY;
        mainCamera.transform.position = new Vector3(minX + (cameraScaleX / 2), minY - (cameraScaleY / 2), -40f);
        mainCamera.transform.localScale = new Vector2(cameraScaleX, cameraScaleY);
    }

    void Update()
    {

    }
}